#! /usr/bin/perl -s

use strict;
use warnings;
use feature 'say';
use LWP::Simple;
use POSIX;

our $server 	= '127.0.0.1' unless defined $server;
our $serverport = '2812' unless defined $serverport;
our $service 	= 'System' unless defined $service;
our $element 	= 'status' unless defined $element;
our $elementstatus = 'Running' unless defined $elementStatus;


my $statusPage=get('http://monit:monit@'.$server.':'.$serverport.'/_status') 
	|| "Failed to get Monit StatusPage";


my $monitOutputString = ''; # see End of File!!
my $monitOutputSvc = '';
my $monitOutputStatus = '';
my $elementKey = '';
my $elementValue = '';


getService($service);



sub getService {
	my $mySvcName=pop @_;

	for my $svcItem (split /^$/m,$statusPage){
		if ($svcItem =~ /$mySvcName/){
			$monitOutputSvc=$svcItem;	# var definition at beginning
			getElement($svcItem);
		}
	}
}

sub getElement {
	#my $mySvc=pop @_;
	#
	for (split /\n/m,pop @_){
		if (/^\s+(\b$element\b)/){ # parantheses for excluding whitespaces from searched string

			$elementKey=$1;
			$elementValue=$';

			# $elementKey;		# not needed when using regex with (...) and $1 ->  =~s/^\s+// ;#and s/\s+$//;
			$elementValue =~ s/^\s+// ;

			#say $elementKey.'####';
			#say $elementValue.'####';
			
			$monitOutputStatus = $elementKey.': '.$elementValue;

		}
	}
}



$monitOutputString= strftime('%y%m%d-%H%M%S',localtime).': '.$monitOutputStatus."\n".$monitOutputSvc;
print $monitOutputString;

$elementValue eq $elementstatus ? exit 0 : exit 99;

